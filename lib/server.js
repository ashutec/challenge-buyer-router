const http = require('http')
const app = require('express')()
const bodyParser = require('body-parser')
const { addBuyer, getBuyer, getRoute } = require('../src/service.js')

const port = '3002'

// add body parser to parse json
app.use(bodyParser.json())

// add routes
app.post('/buyers', addBuyer)
app.get('/buyers/:id', getBuyer)
app.get('/route', getRoute)

// start http server
const server = http.createServer(app)

// export server for testing
module.exports = () => server

// To run stand alone server
if (require.main === module) {
  module.exports().listen(port, function (err) {
    if (!err) {
      console.log('Server started on port %d', port)
    } else {
      console.log(`There is problem starting server: ${err}`)
    }
  })
}
