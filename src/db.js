const client = require('./redis.js')('buyers')
const moment = require('moment')

const addBuyer = buyer => {
  return new Promise((resolve, reject) => {
    if (buyer) {
      client.SET(buyer.id, JSON.stringify(buyer), (error, data) => {
        if (error) {
          reject(error)
        } else {
          createSecondaryIndex(buyer)
          resolve()
        }
      })
    } else {
      reject(new Error('buyer is not given'))
    }
  })
}

const createSecondaryIndex = buyer => {
  let offers = buyer.offers
  offers.map(offer => {
    let score = offer.value
    let location = offer.location
    let states = offer.criteria.state
    let devices = offer.criteria.device
    let days = offer.criteria.day
    let hours = offer.criteria.hour

    devices.map(device => {
      states.map(state => {
        days.map(day => {
          hours.map(hour => {
            let key = `${device}:${state}:${day}:${hour}`
            client.zadd(key, score, location, (error, reply) => {
              if (error) {
                console.log(error)
              }
            })
          })
        })
      })
    })
  })
}

const getBuyer = id => {
  return new Promise((resolve, reject) => {
    if (id) {
      client.GET(id, (error, buyer) => {
        if (error) {
          reject(error)
        } else {
          resolve(JSON.parse(buyer))
        }
      })
    } else {
      reject(new Error('id is not given'))
    }
  })
}

const getRoute = (device, state, timestamp) => {
  return new Promise((resolve, reject) => {
    if (device && state && timestamp) {
      let date = moment(timestamp)
      let day = date.utc().format('e')
      let hour = date.utc().format('H')
      let key = `${device}:${state}:${day}:${hour}`
      client.ZREVRANGE(key, 0, 0, (error, location) => {
        if (error) {
          reject(error)
        } else {
          resolve(location)
        }
      })
    } else {
      reject(new Error('correct no. of parameters are not given'))
    }
  })
}
module.exports = {
  addBuyer: addBuyer,
  getBuyer: getBuyer,
  getRoute: getRoute
}
