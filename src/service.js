const _ = require('lodash')
const db = require('./db.js')

const addBuyer = async (req, res) => {
  let buyer = req.body === null || req.body === undefined ? {} : req.body
  if (_.isEmpty(buyer)) {
    res.status(400).json({})
  } else {
    try {
      await db.addBuyer(buyer)
      res.status(201).json({})
    } catch (error) {
      res.status(400).json({})
    }
  }
}
const getBuyer = async (req, res) => {
  let buyerId = req.params.id
  try {
    let buyer = await db.getBuyer(buyerId)
    res.status(200).json(buyer)
  } catch (error) {
    res.status(400).json({})
  }
}
const getRoute = async (req, res) => {
  let { timestamp, state, device } = req.query
  try {
    let location = await db.getRoute(device, state, timestamp)
    res.status(302).set('location', location).json({})
  } catch (error) {
    res.status(400).json({})
  }
}
module.exports = {
  addBuyer: addBuyer,
  getBuyer: getBuyer,
  getRoute: getRoute
}
